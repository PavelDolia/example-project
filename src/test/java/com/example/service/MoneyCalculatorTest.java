package com.example.service;


import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.pojo.ExchangeRequest;
import com.example.service.persistance.CurrencyService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MoneyCalculatorTest {
    private MoneyCalculator moneyCalculator;

    @BeforeAll
    public void createMoneyCalculator() {
        CurrencyRatesEntity currencyRatesEntity = new CurrencyRatesEntity();
        currencyRatesEntity.setId(1L);
        currencyRatesEntity.setDollarSellingRate(new BigDecimal("28.20000"));
        currencyRatesEntity.setDollarBuyingRate(new BigDecimal("27.80000"));
        currencyRatesEntity.setEuroSellingRate(new BigDecimal("32.20000"));
        currencyRatesEntity.setEuroBuyingRate(new BigDecimal("31.60000"));
        currencyRatesEntity.setDate(new Date());

        CurrencyService currencyService = Mockito.mock(CurrencyService.class);
        Mockito.when(currencyService.findLastCurrencyRates())
                .thenReturn(currencyRatesEntity);
        moneyCalculator = new MoneyCalculator(currencyService);
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnZero_IfNotEnoughToBuyOneUnitOfCurrency() {
        ExchangeRequest exchangeRequest1 = new ExchangeRequest();
        exchangeRequest1.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest1.setCurrencyToBuy(MoneyCalculator.DOLLAR);
        exchangeRequest1.setAmountToSell(new BigDecimal("28.10"));

        ExchangeRequest exchangeRequest2 = new ExchangeRequest();
        exchangeRequest2.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest2.setCurrencyToBuy(MoneyCalculator.EURO);
        exchangeRequest2.setAmountToSell(new BigDecimal("32.10"));
        BigDecimal result1 = moneyCalculator.calculateMoneyToBuy(exchangeRequest1);
        BigDecimal result2 = moneyCalculator.calculateMoneyToBuy(exchangeRequest2);
        Assertions.assertTrue(result1.compareTo(BigDecimal.ZERO) == 0);
        Assertions.assertTrue(result2.compareTo(BigDecimal.ZERO) == 0);
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnOne_IfEnoughToBuyOneAndHalfUnitsOfCurrency() {
        ExchangeRequest exchangeRequest1 = new ExchangeRequest();
        exchangeRequest1.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest1.setCurrencyToBuy(MoneyCalculator.DOLLAR);
        exchangeRequest1.setAmountToSell(new BigDecimal("42.3"));

        ExchangeRequest exchangeRequest2 = new ExchangeRequest();
        exchangeRequest2.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest2.setCurrencyToBuy(MoneyCalculator.EURO);
        exchangeRequest2.setAmountToSell(new BigDecimal("48.3"));
        BigDecimal result1 = moneyCalculator.calculateMoneyToBuy(exchangeRequest1);
        BigDecimal result2 = moneyCalculator.calculateMoneyToBuy(exchangeRequest2);
        Assertions.assertTrue(result1.compareTo(BigDecimal.ONE) == 0);
        Assertions.assertTrue(result2.compareTo(BigDecimal.ONE) == 0);
    }

    @Test()
    public void calculateMoneyToBuy_shouldThrowException_IfIncorrectCurrencyCodes() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            ExchangeRequest exchangeRequest = new ExchangeRequest();
            exchangeRequest.setCurrencyToSell(MoneyCalculator.UAH);
            exchangeRequest.setCurrencyToBuy(MoneyCalculator.UAH);
            moneyCalculator.calculateMoneyToBuy(exchangeRequest);
        });
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnCorrectResult_IfCorrectRequestToBuyDollars() {
        ExchangeRequest exchangeRequest = new ExchangeRequest();
        exchangeRequest.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest.setCurrencyToBuy(MoneyCalculator.DOLLAR);
        exchangeRequest.setAmountToSell(new BigDecimal("2820.00"));
        BigDecimal result = moneyCalculator.calculateMoneyToBuy(exchangeRequest);
        Assertions.assertTrue(result.compareTo(new BigDecimal("100")) == 0);
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnCorrectResult_IfCorrectRequestToSellDollars() {
        ExchangeRequest exchangeRequest = new ExchangeRequest();
        exchangeRequest.setCurrencyToSell(MoneyCalculator.DOLLAR);
        exchangeRequest.setCurrencyToBuy(MoneyCalculator.UAH);
        exchangeRequest.setAmountToSell(new BigDecimal("100.00"));
        BigDecimal result = moneyCalculator.calculateMoneyToBuy(exchangeRequest);
        Assertions.assertTrue(result.compareTo(new BigDecimal("2780")) == 0);
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnCorrectResult_IfCorrectRequestToBuyEuros() {
        ExchangeRequest exchangeRequest = new ExchangeRequest();
        exchangeRequest.setCurrencyToSell(MoneyCalculator.UAH);
        exchangeRequest.setCurrencyToBuy(MoneyCalculator.EURO);
        exchangeRequest.setAmountToSell(new BigDecimal("3220.00"));
        BigDecimal result = moneyCalculator.calculateMoneyToBuy(exchangeRequest);
        Assertions.assertTrue(result.compareTo(new BigDecimal("100")) == 0);
    }

    @Test
    public void calculateMoneyToBuy_shouldReturnCorrectResult_IfCorrectRequestToSellEuros() {
        ExchangeRequest exchangeRequest = new ExchangeRequest();
        exchangeRequest.setCurrencyToSell(MoneyCalculator.EURO);
        exchangeRequest.setCurrencyToBuy(MoneyCalculator.UAH);
        exchangeRequest.setAmountToSell(new BigDecimal("100.00"));
        BigDecimal result = moneyCalculator.calculateMoneyToBuy(exchangeRequest);
        Assertions.assertTrue(result.compareTo(new BigDecimal("3160")) == 0);
    }

}