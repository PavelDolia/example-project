package com.example.service.persistance;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.persistence.repository.CurrencyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class CurrencyServiceImplTest {
    @Autowired
    private CurrencyServiceImpl currencyService;
    @Autowired
    private CurrencyRepository jpaRepository;
    private CurrencyRatesEntity entity = new CurrencyRatesEntity();
    private static final BigDecimal dollarBuyingRate = new BigDecimal("27.75");
    private static final BigDecimal dollarSellingRate = new BigDecimal("28.15");
    private static final BigDecimal euroBuyingRate = new BigDecimal("31.65");
    private static final BigDecimal euroSellingRate = new BigDecimal("32.25");

    @BeforeEach
    public void prepareDataBeforeEach() {
        entity.setId(null);
        entity.setDollarBuyingRate(dollarBuyingRate);
        entity.setDollarSellingRate(dollarSellingRate);
        entity.setEuroBuyingRate(euroBuyingRate);
        entity.setEuroSellingRate(euroSellingRate);
        entity.setDate(new Date());

        jpaRepository.deleteAll();
    }

    @Test
    public void saveCurrencyRates_shouldSaveEntity() {
        CurrencyRatesEntity savedEntity = currencyService.saveCurrencyRates(this.entity);
        Optional<CurrencyRatesEntity> optionalEntity = jpaRepository.findById(savedEntity.getId());
        CurrencyRatesEntity receivedEntity = null;
        if (optionalEntity.isPresent()) {
            receivedEntity = optionalEntity.get();
        }
        Assertions.assertNotNull(savedEntity.getId());
        Assertions.assertTrue(savedEntity.getId() > 0);
        Assertions.assertTrue(optionalEntity.isPresent());
        Assertions.assertTrue(receivedEntity.getDate().toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDate()
                .isEqual(LocalDate.now()));
        Assertions.assertEquals(dollarBuyingRate, receivedEntity.getDollarBuyingRate().stripTrailingZeros());
        Assertions.assertEquals(dollarSellingRate, receivedEntity.getDollarSellingRate().stripTrailingZeros());
        Assertions.assertEquals(euroBuyingRate, receivedEntity.getEuroBuyingRate().stripTrailingZeros());
        Assertions.assertEquals(euroSellingRate, receivedEntity.getEuroSellingRate().stripTrailingZeros());
    }

    @Test
    public void findLastCurrencyRates_shouldReturnNull_ifTableIsEmpty() {
        CurrencyRatesEntity receivedEntity = currencyService.findLastCurrencyRates();
        Assertions.assertNull(receivedEntity);
    }

    @Test
    public void findLastCurrencyRates_shouldReturnEntity_ifTableContainsTodayRecord() {
        currencyService.saveCurrencyRates(entity);
        CurrencyRatesEntity receivedEntity = currencyService.findLastCurrencyRates();
        Assertions.assertNotNull(receivedEntity);
        Assertions.assertTrue(receivedEntity.getDate().toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDate()
                .isEqual(LocalDate.now()));
        Assertions.assertTrue(receivedEntity.getDollarBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getDollarSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getEuroBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getEuroSellingRate().compareTo(BigDecimal.ZERO) > 0);
    }

    @Test
    public void findLastCurrencyRates_shouldReturnEntity_ifTableContainsYesterdayRecord() {
        entity.setDate(java.sql.Timestamp.valueOf((LocalDateTime.now().minusDays(1))));
        currencyService.saveCurrencyRates(entity);
        CurrencyRatesEntity receivedEntity = currencyService.findLastCurrencyRates();
        Assertions.assertNotNull(receivedEntity);
        Assertions.assertTrue(receivedEntity.getDate().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
                .isEqual(LocalDate.now().minusDays(1)));
        Assertions.assertTrue(receivedEntity.getDollarBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getDollarSellingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getEuroBuyingRate().compareTo(BigDecimal.ZERO) > 0);
        Assertions.assertTrue(receivedEntity.getEuroSellingRate().compareTo(BigDecimal.ZERO) > 0);
    }


}