package com.example.service.persistance;

import com.example.persistence.entity.UserEntity;
import com.example.persistence.repository.UserRepository;
import com.example.pojo.User;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
public class UserServiceImplTest {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private User newUser = new User();
    private static final String newUserLogin = "newUser";
    private static final String newUserPassword = "newPassword";

    @BeforeAll
    public void addUser() {
        UserEntity user = new UserEntity(null, "user", "password");
        repository.save(user);
    }

    @BeforeEach
    public void prepareDataBeforeEach() {
        newUser.setLogin(newUserLogin);
        newUser.setPassword(newUserPassword);
        repository.deleteByLogin(newUserLogin);
    }

    @Test
    public void findByLogin_shouldFind() {
        Optional<UserEntity> optionalUser = userService.findByLogin("user");
        Assertions.assertTrue(optionalUser.isPresent());
        Assertions.assertTrue(optionalUser.get().getId() > 0);
        Assertions.assertEquals("user", optionalUser.get().getLogin());
        Assertions.assertEquals("password", optionalUser.get().getPassword());
    }

    @Test
    public void findById_shouldFind() {
        Optional<UserEntity> optionalUser = userService.findById(1L);
        Assertions.assertTrue(optionalUser.isPresent());
        Assertions.assertTrue(optionalUser.get().getId() > 0);
        Assertions.assertEquals("user", optionalUser.get().getLogin());
        Assertions.assertEquals("password", optionalUser.get().getPassword());
    }

    @Test
    public void save_shouldSave_ifNewLogin() {
        userService.save(newUser);
        Optional<UserEntity> optionalUser = userService.findByLogin(newUserLogin);
        Assertions.assertTrue(optionalUser.isPresent());
        Assertions.assertTrue(optionalUser.get().getId() > 0);
        Assertions.assertEquals(newUserLogin, optionalUser.get().getLogin());
        Assertions.assertTrue(passwordEncoder.matches(newUserPassword, optionalUser.get().getPassword()));
    }

    @Test
    public void save_shouldNotSave_ifLoginExists() {
        userService.save(newUser);
        Optional<UserEntity> optionalUser = userService.findByLogin(newUserLogin);
        newUser.setPassword(newUserPassword + "1");
        Optional<UserEntity> savedUser = userService.save(newUser);
        Assertions.assertFalse(savedUser.isPresent());
        Assertions.assertTrue(optionalUser.isPresent());
        Assertions.assertNotEquals(newUserPassword + "1", optionalUser.get().getPassword());
    }

    @Test
    public void deleteById_test() {
        UserEntity savedUser = userService.save(newUser).get();
        userService.deleteById(savedUser.getId());
        Optional<UserEntity> optionalUser = userService.findById(savedUser.getId());
        Assertions.assertFalse(optionalUser.isPresent());
    }

    @Test
    public void deleteByLogin_test() {
        UserEntity user = userService.save(newUser).get();
        userService.deleteByLogin(user.getLogin());
        Optional<UserEntity> optionalUser = userService.findByLogin(user.getLogin());
        Assertions.assertFalse(optionalUser.isPresent());
    }

}