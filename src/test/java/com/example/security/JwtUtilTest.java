package com.example.security;

import com.example.config.SecurityConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

@SpringBootTest(classes = SecurityConfig.class)
@TestPropertySource(locations = "classpath:test.properties")
public class JwtUtilTest {
    @Autowired
    private JwtUtil jwtUtil;

    @Test
    public void generateAccessToken_shouldReturnToken() {
        String token = jwtUtil.generateAccessToken("login");
        Assertions.assertTrue(token.matches("^(.+\\..+\\..+)$"));
    }

    @Test
    public void getLoginFromToken_shouldReturnLogin() {
        String expectedLogin = "login";
        String token = jwtUtil.generateAccessToken(expectedLogin);
        String actualLogin = jwtUtil.getLoginFromToken(token);
        Assertions.assertEquals(expectedLogin, actualLogin);
    }

    @Test
    public void getExpirationDate_shouldReturnDate12HoursInFuture() {
        String token = jwtUtil.generateAccessToken("login");
        LocalDateTime actualDate = jwtUtil.getExpirationDate(token).toInstant()
                .atZone(ZoneId.systemDefault()).toLocalDateTime()
                .truncatedTo(ChronoUnit.MINUTES);
        LocalDateTime expectedDate = LocalDateTime.now().plusHours(12)
                .truncatedTo(ChronoUnit.MINUTES);
        Assertions.assertTrue(expectedDate.isEqual(actualDate));
    }

    @Test
    public void validate_shouldReturnTrue_ifTokenIsValid() {
        String token = jwtUtil.generateAccessToken("login");
        Assertions.assertTrue(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifTokenSignatureIsInvalid() {
        String correctKey = jwtUtil.getKey();
        jwtUtil.setKey("wrongKey");
        String token = jwtUtil.generateAccessToken("login");
        jwtUtil.setKey(correctKey);
        Assertions.assertFalse(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifTokenIsExpired() throws InterruptedException {
        String token = jwtUtil.generateAccessToken("login", 1);
        Thread.sleep(1);
        Assertions.assertFalse(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifItIsNotJwt() throws InterruptedException {
        String token = "jwtUtil.generateAccessToken(\"login\")";
        Assertions.assertFalse(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifTokenIsNull() throws InterruptedException {
        String token = null;
        Assertions.assertFalse(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifTokenIsEmpty() throws InterruptedException {
        String token = "";
        Assertions.assertFalse(jwtUtil.validate(token));
    }

    @Test
    public void validate_shouldReturnFalse_ifTokenIsOnlyWhitespace() throws InterruptedException {
        String token = "   ";
        Assertions.assertFalse(jwtUtil.validate(token));
    }

}