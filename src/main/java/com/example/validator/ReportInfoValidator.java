package com.example.validator;

import com.example.constants.ValidationConstants;
import com.example.pojo.ReportInfo;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ReportInfoValidator implements Validator {
    private String dateRegex = "^([0-9]{4}-[0-9]{2}-[0-9]{2})$";

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == ReportInfo.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ReportInfo request = (ReportInfo) target;
        String fromDate = request.getFromDate();
        String toDate = request.getToDate();
        String currencyCode = request.getCurrencyCode();
        validateFromDate(fromDate, errors);
        validateToDate(toDate, errors);
        validateCurrencyCode(currencyCode, errors);
    }

    private void validateFromDate(String fromDate, Errors errors) {
        if (fromDate == null) {
            errors.rejectValue("fromDate", "", "Enter \"from date\"");
        }
        if (fromDate != null && !fromDate.matches(dateRegex)) {
            errors.rejectValue("fromDate", "", "Incorrect date format");
        }
    }

    private void validateToDate(String toDate, Errors errors) {
        if (toDate == null) {
            errors.rejectValue("toDate", "", "Enter \"to date\"");
        }
        if (toDate != null && !toDate.matches(dateRegex)) {
            errors.rejectValue("toDate", "", "Incorrect date format");
        }
    }

    private void validateCurrencyCode(String currencyCode, Errors errors) {
        if (currencyCode == null) {
            errors.rejectValue("currencyCode", "", "Enter currency code");
        }
        if (currencyCode != null
                && !ValidationConstants.CURRENCY_CODES.contains(currencyCode.toUpperCase())) {
            errors.rejectValue("currencyCode", "", "Incorrect currency code");
        }
    }

}