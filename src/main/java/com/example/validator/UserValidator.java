package com.example.validator;

import com.example.pojo.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == User.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        if (StringUtils.isBlank(user.getLogin())) {
            errors.rejectValue("login", "", "Enter login");
        }
        if (user.getLogin() != null && user.getLogin().length() > 50) {
            errors.rejectValue("login", "", "Login mustn't be longer than 50 characters");
        }
        if (StringUtils.isBlank(user.getPassword())) {
            errors.rejectValue("password", "", "Enter password");
        }
    }

}