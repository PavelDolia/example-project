package com.example.service;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.pojo.*;
import com.example.service.persistance.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class MainControllerService {
    private CurrencyRatesReceiver currencyRatesReceiver;
    private MoneyCalculator moneyCalculator;
    private RequestService requestService;
    private EmailService emailService;

    public MainControllerService(CurrencyRatesReceiver currencyRatesReceiver, MoneyCalculator moneyCalculator,
                                 RequestService requestService, EmailService emailService) {
        this.currencyRatesReceiver = currencyRatesReceiver;
        this.moneyCalculator = moneyCalculator;
        this.requestService = requestService;
        this.emailService = emailService;
    }

    public ResponseEntity saveCurrencyRates() {
        ResponseEntity result;
        CurrencyRatesEntity currencyRatesEntity = currencyRatesReceiver.saveCurrencyRates();
        if (currencyRatesEntity != null) {
            CurrencyRates currencyRates = new CurrencyRates(currencyRatesEntity);
            result = ResponseEntity.ok().body(currencyRates);
        } else {
            result = ResponseEntity.internalServerError().body(new Message("Unable to receive currency rates"));
        }
        return result;
    }

    public ResponseEntity createCurrencyExchangeRequest(ExchangeRequest request) {
        String otp = OtpCreator.generateOtp();
        BigDecimal moneyToBuy = moneyCalculator.calculateMoneyToBuy(request);
        ExchangeRequestEntity requestEntity = new ExchangeRequestEntity(request, otp, moneyToBuy);
        requestEntity = requestService.saveOrUpdate(requestEntity);
        emailService.sendSimpleMessage(request.getEmail(), "OTP password",
                "Your code for a currency exchange transaction - " + otp);
        RequestResponse requestResponse = new RequestResponse(moneyToBuy, request.getPhoneNumber(),
                otp, requestEntity.getId());
        return ResponseEntity.ok().body(requestResponse);
    }

    public ResponseEntity confirmCurrencyExchangeRequest(ConfirmInfo confirmInfo) {
        Optional<ExchangeRequestEntity> optionalRequest = requestService
                .findById(confirmInfo.getId());
        if (!optionalRequest.isPresent()) {
            return ResponseEntity.badRequest().body(new Message("Incorrect id, " +
                    "unable to find a request with this id at a database"));
        }
        ExchangeRequestEntity requestEntity = optionalRequest.get();
        ResponseEntity result;
        if (requestEntity.getOtp().equals(confirmInfo.getOtp())) {
            requestService.updateStatus(requestEntity.getId(), "done");
            result = ResponseEntity.ok(new Message("The request has been confirmed"));
        } else {
            requestService.updateStatus(requestEntity.getId(), "canceled");
            result = ResponseEntity.status(406).body(new Message("OTP doesn't match. The request has been canceled"));
        }
        return result;
    }

    public ResponseEntity deleteRequest(Long id) {
        ResponseEntity result;
        if (requestService.delete(id)) {
            result = ResponseEntity.ok(new Message("The request has been deleted"));
        } else {
            result = ResponseEntity.status(406).body(new Message("Unable to delete a request, " +
                    "it doesn't exist or has been confirmed/canceled"));
        }
        return result;
    }

    public RequestService getRequestService() {
        return requestService;
    }

    public void setRequestService(RequestService requestService) {
        this.requestService = requestService;
    }

}