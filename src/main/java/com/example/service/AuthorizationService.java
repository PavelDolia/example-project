package com.example.service;

import com.example.persistence.entity.UserEntity;
import com.example.pojo.Message;
import com.example.pojo.User;
import com.example.security.JwtUtil;
import com.example.service.persistance.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthorizationService {
    private UserService userService;
    private PasswordEncoder passwordEncoder;
    private JwtUtil jwtUtil;

    public AuthorizationService(UserService userService,
                                PasswordEncoder passwordEncoder,
                                JwtUtil jwtUtil) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtil = jwtUtil;
    }

    public ResponseEntity login(User user) {
        ResponseEntity response;
        Optional<UserEntity> optionalUser = userService.findByLogin(user.getLogin());
        if (optionalUser.isPresent()
                && passwordEncoder.matches(user.getPassword(), optionalUser.get().getPassword())) {
            response = ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION,
                            "Bearer " + jwtUtil.generateAccessToken(user.getLogin())).build();
        } else {
            response = ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Incorrect login or password");
        }
        return response;
    }

    public ResponseEntity register(User user) {
        ResponseEntity response;
        Optional<UserEntity> savedUser = userService.save(user);
        if (savedUser.isPresent()) {
            response = ResponseEntity.ok(new Message("User has been saved"));
        } else {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Message("User already exists"));
        }
        return response;
    }

}