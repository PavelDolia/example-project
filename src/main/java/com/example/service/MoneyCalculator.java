package com.example.service;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.pojo.ExchangeRequest;
import com.example.service.persistance.CurrencyService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class MoneyCalculator {
    private CurrencyService currencyService;
    public static final String DOLLAR = "USD";
    public static final String EURO = "EUR";
    public static final String UAH = "UAH";

    public MoneyCalculator(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public BigDecimal calculateMoneyToBuy(ExchangeRequest request) {
        BigDecimal result;
        BigDecimal currencyRate;
        String currencyToSell = request.getCurrencyToSell();
        String currencyToBuy = request.getCurrencyToBuy();
        CurrencyRatesEntity currencyRates = currencyService.findLastCurrencyRates();

        if (currencyToSell.equalsIgnoreCase(UAH)
                && currencyToBuy.equalsIgnoreCase(DOLLAR)) {
            currencyRate = currencyRates.getDollarSellingRate();
            result = request.getAmountToSell().abs().divide(currencyRate, 2, BigDecimal.ROUND_FLOOR);
        } else if (currencyToSell.equalsIgnoreCase(DOLLAR)
                && currencyToBuy.equalsIgnoreCase(UAH)) {
            currencyRate = currencyRates.getDollarBuyingRate();
            result = request.getAmountToSell().abs().multiply(currencyRate);
        } else if (currencyToSell.equalsIgnoreCase(UAH)
                && currencyToBuy.equalsIgnoreCase(EURO)) {
            currencyRate = currencyRates.getEuroSellingRate();
            result = request.getAmountToSell().abs().divide(currencyRate, 2, BigDecimal.ROUND_FLOOR);
        } else if (currencyToSell.equalsIgnoreCase(EURO)
                && currencyToBuy.equalsIgnoreCase(UAH)) {
            currencyRate = currencyRates.getEuroBuyingRate();
            result = request.getAmountToSell().abs().multiply(currencyRate);
        } else {
            throw new IllegalArgumentException("The currency to buy code is incorrect" +
                    " or the currency to sell code is incorrect");
        }
        return result.setScale(0, BigDecimal.ROUND_DOWN);
    }

}