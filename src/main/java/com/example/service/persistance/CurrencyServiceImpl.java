package com.example.service.persistance;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.persistence.repository.CurrencyRepository;
import org.springframework.stereotype.Component;

@Component
public class CurrencyServiceImpl implements CurrencyService {
    private CurrencyRepository repository;

    public CurrencyServiceImpl(CurrencyRepository repository) {
        this.repository = repository;
    }

    @Override
    public CurrencyRatesEntity saveCurrencyRates(CurrencyRatesEntity currencyRatesEntity) {
        return repository.save(currencyRatesEntity);
    }

    @Override
    public CurrencyRatesEntity findLastCurrencyRates() {
        return repository.findLastCurrencyRates();
    }

}