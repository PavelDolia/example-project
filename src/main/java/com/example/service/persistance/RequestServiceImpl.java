package com.example.service.persistance;

import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.persistence.repository.RequestRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;


@Component
@Transactional
public class RequestServiceImpl implements RequestService {
    private RequestRepository repository;

    public RequestServiceImpl(RequestRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExchangeRequestEntity saveOrUpdate(ExchangeRequestEntity request) {
        return repository.save(request);

    }

    @Override
    public void updateStatus(Long id, String status) {
        repository.updateStatus(id, status);
    }

    @Override
    public boolean delete(Long id) {
        int changedRows = repository.deleteIfStatusIsNew(id);
        return changedRows > 0;
    }

    @Override
    public Optional<ExchangeRequestEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<ExchangeRequestEntity> findDoneTodayRequests() {
        LocalDateTime from = LocalDate.now().atStartOfDay();        //the beginning of the current day.
        LocalDateTime to = LocalDate.now().atTime(LocalTime.MAX);   //the end of the current day
        return repository.findDoneRequestsBetweenDates(from, to);
    }

    @Override
    public List<ExchangeRequestEntity> findDoneRequestsForCurrencyBetweenDates(LocalDateTime from,
                                                                               LocalDateTime to,
                                                                               String currencyCode) {
        return repository.findDoneRequestsForCurrencyBetweenDates(from, to, currencyCode);
    }

}