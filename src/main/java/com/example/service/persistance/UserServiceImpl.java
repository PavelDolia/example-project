package com.example.service.persistance;

import com.example.persistence.entity.UserEntity;
import com.example.persistence.repository.UserRepository;
import com.example.pojo.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@Transactional
public class UserServiceImpl implements UserService {
    private UserRepository repository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<UserEntity> findByLogin(String login) {
        return repository.findByLogin(login);
    }

    @Override
    public Optional<UserEntity> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<UserEntity> save(User user) {
        if (findByLogin(user.getLogin()).isPresent()) {
            return Optional.empty();
        }
        UserEntity entity = new UserEntity();
        entity.setLogin(user.getLogin());
        entity.setPassword(passwordEncoder.encode(user.getPassword()));
        entity = repository.save(entity);
        return Optional.of(entity);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void deleteByLogin(String login) {
        repository.deleteByLogin(login);
    }

}