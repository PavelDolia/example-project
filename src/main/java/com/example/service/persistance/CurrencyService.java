package com.example.service.persistance;

import com.example.persistence.entity.CurrencyRatesEntity;

public interface CurrencyService {

    CurrencyRatesEntity saveCurrencyRates(CurrencyRatesEntity currencyRatesEntity);

    CurrencyRatesEntity findLastCurrencyRates();

}