package com.example.service;

import com.example.persistence.entity.ExchangeRequestEntity;
import com.example.pojo.*;
import com.example.service.persistance.RequestService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ReportBuilder {
    private RequestService requestService;
    private BigDecimal dollarSold;
    private BigDecimal dollarBought;
    private BigDecimal euroSold;
    private BigDecimal euroBought;
    private BigDecimal uahSold;
    private BigDecimal uahBought;

    public ReportBuilder(RequestService requestService) {
        this.requestService = requestService;
    }

    /**
     * Returns the object that has information about today's transactions:
     * - today's date;
     * - total amount of currency exchange transactions;
     * - total amount of each type of currency that has been sold and bought.
     *
     * @return an object that has information about today's transactions
     */
    public TodaysReport buildTodayReport() {
        List<ExchangeRequestEntity> requests = requestService.findDoneTodayRequests();
        calculateValues(requests);
        CurrencyReport dollarReport = new CurrencyReport(dollarSold, dollarBought, MoneyCalculator.DOLLAR);
        CurrencyReport euroReport = new CurrencyReport(euroSold, euroBought, MoneyCalculator.EURO);
        CurrencyReport uahReport = new CurrencyReport(uahSold, uahBought, MoneyCalculator.UAH);
        List<CurrencyReport> reports = new ArrayList<>();
        reports.add(dollarReport);
        reports.add(euroReport);
        reports.add(uahReport);
        return new TodaysReport(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                requests.size(), reports);
    }

    /**
     * Sets the initial values for today's report
     */
    private void resetValues() {
        dollarSold = new BigDecimal(0);
        dollarBought = new BigDecimal(0);
        euroSold = new BigDecimal(0);
        euroBought = new BigDecimal(0);
        uahSold = new BigDecimal(0);
        uahBought = new BigDecimal(0);
    }

    /**
     * Calculates the total amount of each type of currency that has been sold and bought
     *
     * @param requests a list of all currency exchange transactions
     */
    private void calculateValues(List<ExchangeRequestEntity> requests) {
        resetValues();
        for (ExchangeRequestEntity request : requests) {
            String currencyToSellCode = request.getCurrencyToSell();
            String currencyToBuyCode = request.getCurrencyToBuy();
            if (currencyToSellCode.equalsIgnoreCase(MoneyCalculator.UAH)) {
                uahBought = uahBought.add(request.getAmountToSell());
            } else if (currencyToSellCode.equalsIgnoreCase(MoneyCalculator.DOLLAR)) {
                dollarBought = dollarBought.add(request.getAmountToSell());
            } else if (currencyToSellCode.equalsIgnoreCase(MoneyCalculator.EURO)) {
                euroBought = euroBought.add(request.getAmountToSell());
            }
            if (currencyToBuyCode.equalsIgnoreCase(MoneyCalculator.DOLLAR)) {
                dollarSold = dollarSold.add(request.getAmountToBuy());
            } else if (currencyToBuyCode.equalsIgnoreCase(MoneyCalculator.UAH)) {
                uahSold = uahSold.add(request.getAmountToBuy());
            } else if (currencyToBuyCode.equalsIgnoreCase(MoneyCalculator.EURO)) {
                euroSold = euroSold.add(request.getAmountToBuy());
            }
        }
    }

    /**
     * Returns the object that has information for the selected period:
     * - currency code;
     * - the total amount of currency that has been sold for the period;
     * - the total amount of currency that has been bought for the period;
     * - the total amount of currency exchange transactions for the period;
     * - a list of reports for each day. These reports have the same information as mentioned above.
     *
     * @param reportInfo an object with requirements for the report. It has a starting date,
     *                   an ending date and a currency code.
     * @return an object that has a report about the selected type of currency for the specified period
     */
    public ReportForPeriod buildReportForPeriod(ReportInfo reportInfo) {
        String currencyCode = reportInfo.getCurrencyCode().toUpperCase();
        List<ExchangeRequestEntity> allRequests = findNeededRequests(reportInfo);
        if (allRequests.size() == 0) {
            return emptyReportForPeriod(currencyCode);
        }
        List<List<ExchangeRequestEntity>> requestsByDays = createRequestsByDaysList(allRequests);
        return createReportForPeriod(requestsByDays, currencyCode);
    }

    /**
     * Returns a list of completed currency exchange requests for the selected period
     *
     * @param reportInfo an object with requirements for the report. It has a starting date,
     *                   an ending date and a currency code.
     * @return a list of completed currency exchange requests for the selected period
     */
    private List<ExchangeRequestEntity> findNeededRequests(ReportInfo reportInfo) {
        LocalDateTime from = LocalDate.parse(reportInfo.getFromDate(), DateTimeFormatter.ISO_LOCAL_DATE)
                .atStartOfDay();
        LocalDateTime to = LocalDate.parse(reportInfo.getToDate(), DateTimeFormatter.ISO_LOCAL_DATE)
                .atTime(LocalTime.MAX);
        String currencyCode = reportInfo.getCurrencyCode().toUpperCase();
        return requestService.findDoneRequestsForCurrencyBetweenDates(from, to, currencyCode);
    }

    private ReportForPeriod emptyReportForPeriod(String currencyCode) {
        ReportForPeriod report = new ReportForPeriod();
        report.setCurrencyCode(currencyCode);
        report.setSoldTotal(BigDecimal.ZERO);
        report.setBoughtTotal(BigDecimal.ZERO);
        report.setTotalTransactions(0);
        report.setDayReports(new ArrayList<>());
        return report;
    }

    /**
     * To ease further calculations, it creates a list that contains lists
     * of completed currency exchange requests for the same days.
     *
     * @param allRequests a list of completed currency exchange requests
     * @return a list that contains lists of completed currency exchange requests for the same days
     */
    private List<List<ExchangeRequestEntity>> createRequestsByDaysList(List<ExchangeRequestEntity> allRequests) {
        List<List<ExchangeRequestEntity>> requestsByDays = new ArrayList<>();
        List<ExchangeRequestEntity> sameDayRequests = new ArrayList<>();
        sameDayRequests.add(allRequests.get(0));
        if (allRequests.size() == 1) {
            requestsByDays.add(sameDayRequests);
        }
        for (int i = 1; i < allRequests.size(); i++) {
            if (!allRequests.get(i).getDate().toLocalDate()
                    .isEqual(sameDayRequests.get(0).getDate().toLocalDate())) {
                requestsByDays.add(sameDayRequests);
                sameDayRequests = new ArrayList<>();
            }
            sameDayRequests.add(allRequests.get(i));
            if (i == allRequests.size() - 1
                    && !sameDayRequests.get(0).getDate().toLocalDate()
                    .isEqual(requestsByDays.get(requestsByDays.size() - 1).get(0).getDate().toLocalDate())) {
                requestsByDays.add(sameDayRequests);
            }
        }
        return requestsByDays;
    }

    /**
     * Creates a report about one type of currency for a certain period.
     *
     * @param requestsByDays a list that contains lists of completed currency exchange requests for the same days
     * @param currencyCode   a currency code
     * @return a report about one type of currency for a certain period
     */
    private ReportForPeriod createReportForPeriod(List<List<ExchangeRequestEntity>> requestsByDays,
                                                  String currencyCode) {
        List<DayReport> dayReportList = new ArrayList<>();
        BigDecimal soldTotal = new BigDecimal(0);
        BigDecimal boughtTotal = new BigDecimal(0);
        int totalTransactions = 0;

        for (List<ExchangeRequestEntity> sameDayRequestEntities : requestsByDays) {
            totalTransactions += sameDayRequestEntities.size();
            DayReport dayReport = new DayReport();
            dayReport.setTransactionCount(sameDayRequestEntities.size());
            dayReport.setDate(sameDayRequestEntities.get(0).getDate().toLocalDate());
            dayReport.setCurrencyCode(currencyCode);
            BigDecimal soldInDay = new BigDecimal(0);
            BigDecimal boughtInDay = new BigDecimal(0);
            for (ExchangeRequestEntity entity : sameDayRequestEntities) {
                if (entity.getCurrencyToSell().equalsIgnoreCase(currencyCode)) {
                    boughtInDay = boughtInDay.add(entity.getAmountToSell());
                } else {
                    soldInDay = soldInDay.add(entity.getAmountToBuy());
                }
            }
            dayReport.setBought(boughtInDay);
            dayReport.setSold(soldInDay);
            dayReportList.add(dayReport);
            soldTotal = soldTotal.add(soldInDay);
            boughtTotal = boughtTotal.add(boughtInDay);
        }
        return new ReportForPeriod(currencyCode, soldTotal, boughtTotal,
                totalTransactions, dayReportList);
    }

}