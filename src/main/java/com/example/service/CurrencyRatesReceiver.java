package com.example.service;

import com.example.persistence.entity.CurrencyRatesEntity;
import com.example.pojo.CurrencyRate;
import com.example.service.persistance.CurrencyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Component
public class CurrencyRatesReceiver {
    @Value("${currency.rates.url}")
    private String url;
    private CurrencyService currencyService;
    private RestTemplate restTemplate = new RestTemplate();
    private static final String USD = "USD";
    private static final String EUR = "EUR";

    public CurrencyRatesReceiver(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public CurrencyRatesEntity saveCurrencyRates() {
        CurrencyRatesEntity currencyRates = getCurrencyRates();
        if (currencyRates != null && isValid(currencyRates)) {
            currencyService.saveCurrencyRates(currencyRates);
            return currencyRates;
        } else {
            return null;
        }
    }

    private CurrencyRatesEntity getCurrencyRates() {
        ResponseEntity<CurrencyRate[]> response = restTemplate.getForEntity(url, CurrencyRate[].class);
        CurrencyRate[] rates = response.getBody();
        if (rates != null) {
            return buildCurrencyEntity(rates);
        } else {
            return null;
        }
    }

    private CurrencyRatesEntity buildCurrencyEntity(CurrencyRate[] rates) {
        CurrencyRatesEntity result = new CurrencyRatesEntity();
        for (CurrencyRate rate : rates) {
            if (rate.getCurrencyCode().equals(USD)) {
                result.setDollarBuyingRate(rate.getBuyingRate());
                result.setDollarSellingRate(rate.getSellingRate());
            }
            if (rate.getCurrencyCode().equals(EUR)) {
                result.setEuroBuyingRate(rate.getBuyingRate());
                result.setEuroSellingRate(rate.getSellingRate());
            }
        }
        return result;
    }

    private boolean isValid(CurrencyRatesEntity currencyRates) {
        return currencyRates.getDollarBuyingRate().compareTo(BigDecimal.ZERO) > 0
                && currencyRates.getDollarSellingRate().compareTo(BigDecimal.ZERO) > 0
                && currencyRates.getEuroBuyingRate().compareTo(BigDecimal.ZERO) > 0
                && currencyRates.getEuroSellingRate().compareTo(BigDecimal.ZERO) > 0;
    }

}