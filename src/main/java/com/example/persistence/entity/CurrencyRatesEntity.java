package com.example.persistence.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "currencyRates")
public class CurrencyRatesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(precision = 10, scale = 5)
    private BigDecimal dollarSellingRate;

    @Column(precision = 10, scale = 5)
    private BigDecimal dollarBuyingRate;

    @Column(precision = 10, scale = 5)
    private BigDecimal euroSellingRate;

    @Column(precision = 10, scale = 5)
    private BigDecimal euroBuyingRate;

    @Column(name = "date", nullable = false)
    private Date date;

    public CurrencyRatesEntity() {
        dollarSellingRate = BigDecimal.ZERO;
        dollarBuyingRate = BigDecimal.ZERO;
        euroSellingRate = BigDecimal.ZERO;
        euroBuyingRate = BigDecimal.ZERO;
        date = new Date();
    }

    public BigDecimal getDollarSellingRate() {
        return dollarSellingRate;
    }

    public void setDollarSellingRate(BigDecimal dollarSellingRate) {
        this.dollarSellingRate = dollarSellingRate;
    }

    public BigDecimal getDollarBuyingRate() {
        return dollarBuyingRate;
    }

    public void setDollarBuyingRate(BigDecimal dollarBuyingRate) {
        this.dollarBuyingRate = dollarBuyingRate;
    }

    public BigDecimal getEuroSellingRate() {
        return euroSellingRate;
    }

    public void setEuroSellingRate(BigDecimal euroSellingRate) {
        this.euroSellingRate = euroSellingRate;
    }

    public BigDecimal getEuroBuyingRate() {
        return euroBuyingRate;
    }

    public void setEuroBuyingRate(BigDecimal euroBuyingRate) {
        this.euroBuyingRate = euroBuyingRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}