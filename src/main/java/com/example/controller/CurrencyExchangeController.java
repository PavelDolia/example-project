package com.example.controller;

import com.example.pojo.*;
import com.example.service.MainControllerService;
import com.example.service.ReportBuilder;
import com.example.validator.ConfirmInfoValidator;
import com.example.validator.ExchangeRequestValidator;
import com.example.validator.ReportInfoValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/exchange")
@Tag(name = "Currency exchange controller",
        description = "It is used to create, confirm, and delete " +
                "currency exchange requests and to show reports." +
                "<br>Each method requires an authorization header with a token.")
public class CurrencyExchangeController {
    private MainControllerService mainControllerService;

    public CurrencyExchangeController(MainControllerService mainControllerService) {
        this.mainControllerService = mainControllerService;
    }

    @InitBinder("exchangeRequest")
    public void requestInitBinder(WebDataBinder binder) {
        binder.setValidator(new ExchangeRequestValidator());
    }

    @InitBinder("confirmInfo")
    public void confirmInitBinder(WebDataBinder binder) {
        binder.setValidator(new ConfirmInfoValidator());
    }

    @InitBinder("reportInfo")
    public void reportInitBinder(WebDataBinder binder) {
        binder.setValidator(new ReportInfoValidator());
    }


    @Operation(summary = "Starts the application",
            description = "This method must be invoked first before all other methods" +
                    " of the currency exchange controller." +
                    "<br>The method saves currency rates. The response with the http code 200 " +
                    "is mandatory for the correct work of a server.",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If your authorization token is present and valid",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = CurrencyRates.class))),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content),
                    @ApiResponse(responseCode = "500", description = "If unexpected errors occur",
                            content = @Content(mediaType = "application/json",
                                    examples = @ExampleObject(value = "{''message'': " +
                                            "''Unable to receive currency rates''}")))
            })
    @GetMapping("/start")
    public ResponseEntity startWorkingDay() {
        return mainControllerService.saveCurrencyRates();
    }


    @Operation(summary = "Creates a new currency exchange request",
            description = "See 'ExchangeRequest' in section 'Schemas' to create valid input data",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If your authorization token is present and valid",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = RequestResponse.class))
                            }),
                    @ApiResponse(responseCode = "400", description = "If the request body is invalid",
                            content = @Content),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content)
            })
    @PostMapping("/request/new")
    public ResponseEntity createCurrencyExchangeRequest(@RequestBody @Validated ExchangeRequest request) {
        return mainControllerService.createCurrencyExchangeRequest(request);
    }


    @Operation(summary = "Confirms a currency exchange request",
            description = "See 'ConfirmInfo' in section 'Schemas' to create valid input data." +
                    "<br>If an OTP doesn't match, request's status is changed to 'canceled'. " +
                    "After that it's impossible to confirm this request.",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If the OTP matches",
                            content = @Content(mediaType = "application/json",
                                    examples = @ExampleObject(value = "{''message'': " +
                                            "''The request has been confirmed''}"))),
                    @ApiResponse(responseCode = "406",
                            description = "If the OTP doesn't match, the request's status is changed to 'canceled'. " +
                                    "After that it's impossible to confirm this request.",
                            content = @Content(mediaType = "application/json",
                                    examples = @ExampleObject(value = "{''message'': " +
                                            "''OTP doesn't match. The request has been canceled''}"))),
                    @ApiResponse(responseCode = "400", description = "If the request body is invalid",
                            content = @Content),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content)
            })
    @PutMapping("/request/confirm")
    public ResponseEntity confirmCurrencyExchangeRequest(@RequestBody @Validated ConfirmInfo confirmInfo) {
        return mainControllerService.confirmCurrencyExchangeRequest(confirmInfo);
    }

    @Operation(summary = "Deletes a currency exchange request",
            description = "Deletes a request by its identification number",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If the identification number is correct",
                            content = @Content(mediaType = "application/json",
                                    examples = @ExampleObject(value = "{''message'': " +
                                            "''The request has been deleted''}"))),
                    @ApiResponse(responseCode = "406",
                            description = "If the identification number is incorrect",
                            content = @Content(mediaType = "application/json",
                                    examples = @ExampleObject(value = "{''message'': ''Unable to delete a request, " +
                                            "it doesn't exist or has been confirmed/canceled''}"))),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content)
            })
    @DeleteMapping("/request/delete/{id}")
    public ResponseEntity deleteRequest(@Schema(example = "1") @PathVariable Long id) {
        return mainControllerService.deleteRequest(id);
    }

    @Operation(summary = "Gives today's transactions report",
            description = "Gives information about the total number of currency exchange transactions" +
                    "<br>and how many currency units of each type were bought and sold today.",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If your authorization token is present and valid",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = TodaysReport.class))),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content)
            })
    @GetMapping("/report")
    public ResponseEntity getTodayReport() {
        ReportBuilder reportBuilder = new ReportBuilder(mainControllerService.getRequestService());
        TodaysReport todaysReport = reportBuilder.buildTodayReport();
        return ResponseEntity.ok(todaysReport);
    }

    @Operation(summary = "Gives a currency exchange transactions report between two dates",
            description = "Gives information about one type of currency, such as " +
                    "<br>the total number of transactions, " +
                    "the total amount of currency that has been sold and bought, " +
                    "<br>and reports that have the same information for each day.",
            security = @SecurityRequirement(name = "Authorization"),
            responses = {
                    @ApiResponse(responseCode = "200", description = "If input data are valid",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ReportForPeriod.class))),
                    @ApiResponse(responseCode = "400", description = "If input data are invalid",
                            content = @Content),
                    @ApiResponse(responseCode = "403", description = "If your authorization token is absent or invalid",
                            content = @Content)
            })
    @PostMapping("/report/between/dates")
    public ResponseEntity getReportBetweenDates(@RequestBody @Validated ReportInfo reportInfo) {
        ReportBuilder reportBuilder = new ReportBuilder(mainControllerService.getRequestService());
        ReportForPeriod reportForPeriod = reportBuilder.buildReportForPeriod(reportInfo);
        return ResponseEntity.ok(reportForPeriod);
    }

}