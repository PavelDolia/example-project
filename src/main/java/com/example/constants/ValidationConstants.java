package com.example.constants;

import com.example.service.MoneyCalculator;

import java.util.HashSet;
import java.util.Set;

public abstract class ValidationConstants {
    public static final Set<String> CURRENCY_CODES = getCurrencyCodes();

    private static Set<String> getCurrencyCodes() {
        Set<String> currencyCodes = new HashSet<>();
        currencyCodes.add(MoneyCalculator.DOLLAR);
        currencyCodes.add(MoneyCalculator.EURO);
        currencyCodes.add(MoneyCalculator.UAH);
        return currencyCodes;
    }

}