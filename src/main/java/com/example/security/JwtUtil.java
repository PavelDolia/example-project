package com.example.security;

import io.jsonwebtoken.*;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
public class JwtUtil {
    private Logger logger = Logger.getLogger(JwtUtil.class);

    @Value("${jwt.key}")
    private transient String key;

    public String generateAccessToken(String login) {
        return Jwts.builder()
                .setIssuedAt(new Date())
                .claim("login", login)
                .setExpiration(Date.from(LocalDateTime.now().plusHours(12).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }

    public String generateAccessToken(String login, int nanoseconds) {
        return Jwts.builder()
                .setIssuedAt(new Date())
                .claim("login", login)
                .setExpiration(Date.from(LocalDateTime.now().plusNanos(nanoseconds).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }

    public String getLoginFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody();
        return claims.get("login", String.class);
    }

    public Date getExpirationDate(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody();
        return claims.getExpiration();
    }

    public boolean validate(String token) {
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            return true;
        } catch (SignatureException ex) {
            logger.log(Level.INFO, "Invalid JWT signature", ex);
        } catch (MalformedJwtException ex) {
            logger.log(Level.INFO, "Invalid JWT token", ex);
        } catch (ExpiredJwtException ex) {
            logger.log(Level.INFO, "Expired JWT token", ex);
        } catch (UnsupportedJwtException ex) {
            logger.log(Level.INFO, "Unsupported JWT token", ex);
        } catch (IllegalArgumentException ex) {
            logger.log(Level.INFO, "JWT claims string is empty", ex);
        } catch (Exception ex) {
            logger.log(Level.INFO, "Invalid token", ex);
        }
        return false;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}