package com.example.pojo;

import com.example.persistence.entity.CurrencyRatesEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CurrencyRates {
    @Schema(description = "a date in the format dd.MM.yyyy")
    private String date;
    private List<CurrencyRate> currencyRates = new ArrayList<>();

    public CurrencyRates() {
    }

    public CurrencyRates(CurrencyRatesEntity currencyRatesEntity) {
        CurrencyRate dollar = new CurrencyRate();
        CurrencyRate euro = new CurrencyRate();
        dollar.setCurrencyCode("USD");
        dollar.setSellingRate(currencyRatesEntity.getDollarSellingRate().stripTrailingZeros());
        dollar.setBuyingRate(currencyRatesEntity.getDollarBuyingRate().stripTrailingZeros());
        euro.setCurrencyCode("EUR");
        euro.setSellingRate(currencyRatesEntity.getEuroSellingRate().stripTrailingZeros());
        euro.setBuyingRate(currencyRatesEntity.getEuroBuyingRate().stripTrailingZeros());

        date = new SimpleDateFormat("dd.MM.yyyy")
                .format(currencyRatesEntity.getDate());
        currencyRates.add(dollar);
        currencyRates.add(euro);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CurrencyRate> getCurrencyRates() {
        return currencyRates;
    }

    public void setCurrencyRates(List<CurrencyRate> currencyRates) {
        this.currencyRates = currencyRates;
    }

}