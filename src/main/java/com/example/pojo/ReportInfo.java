package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

public class ReportInfo {
    @JsonProperty("from")
    @Schema(description = "the starting date of the report in the format yyyy-MM-dd", example = "2022-01-30")
    private String fromDate;

    @JsonProperty("to")
    @Schema(description = "the ending date of the report in the format yyyy-MM-dd", example = "2022-02-14")
    private String toDate;

    @JsonProperty("code")
    @Schema(description = "a currency code", example = "USD, EUR or UAH")
    private String currencyCode;

    public ReportInfo() {
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String code) {
        this.currencyCode = code;
    }

}