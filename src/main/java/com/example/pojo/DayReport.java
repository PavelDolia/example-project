package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;
import java.time.LocalDate;

@JsonPropertyOrder({"date", "ccy", "sold", "bought", "transactionCount"})
public class DayReport {
    @Schema(description = "the total amount of currency that has been sold on the same day")
    private BigDecimal sold;

    @Schema(description = "the total amount of currency that has been bought on the same day")
    private BigDecimal bought;

    @Schema(description = "the total number of currency exchange transactions on the same day")
    private int transactionCount;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(description = "a date in the format yyyy-MM-dd", example = "2022-01-30")
    private LocalDate date;

    @JsonProperty("ccy")
    @Schema(description = "a currency code", example = "USD, EUR or UAH")
    private String currencyCode;

    public DayReport() {
        sold = BigDecimal.ZERO;
        bought = BigDecimal.ZERO;
        transactionCount = 0;
    }

    public DayReport(BigDecimal sold, BigDecimal bought, int transactionCount,
                     LocalDate date, String currencyCode) {
        this.sold = sold;
        this.bought = bought;
        this.transactionCount = transactionCount;
        this.date = date;
        this.currencyCode = currencyCode;
    }

    public BigDecimal getSold() {
        return sold;
    }

    public void setSold(BigDecimal sold) {
        this.sold = sold;
    }

    public BigDecimal getBought() {
        return bought;
    }

    public void setBought(BigDecimal bought) {
        this.bought = bought;
    }

    public int getTransactionCount() {
        return transactionCount;
    }

    public void setTransactionCount(int transactionCount) {
        this.transactionCount = transactionCount;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}