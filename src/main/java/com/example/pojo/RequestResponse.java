package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

public class RequestResponse {
    @JsonProperty("money")
    @Schema(description = "an amount of money that a customer receives")
    private BigDecimal moneyAmountToBuy;

    @Schema(description = "a set of 9 or 10 digits", example = "0973742713 or 973742713")
    private String phoneNumber;

    @Schema(description = "a password to confirm a request. It contains 6 digits", example = "123456")
    private String otp;

    @Schema(description = "the identification number for the request")
    private Long id;

    public RequestResponse() {
    }

    public RequestResponse(BigDecimal moneyAmountToBuy,
                           String phoneNumber, String otp, Long id) {
        this.moneyAmountToBuy = moneyAmountToBuy;
        this.phoneNumber = phoneNumber;
        this.otp = otp;
        this.id = id;
    }

    public BigDecimal getMoneyAmountToBuy() {
        return moneyAmountToBuy;
    }

    public void setMoneyAmountToBuy(BigDecimal moneyAmountToBuy) {
        this.moneyAmountToBuy = moneyAmountToBuy;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}