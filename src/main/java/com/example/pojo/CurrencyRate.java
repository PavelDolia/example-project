package com.example.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

public class CurrencyRate {
    @JsonProperty(value = "ccy")
    @Schema(description = "a currency code", example = "USD, EUR or UAH")
    private String currencyCode;

    @JsonProperty(value = "buy")
    @Schema(description = "the buying rate of the currency for a money changer", example = "28.05000")
    private BigDecimal buyingRate;

    @JsonProperty(value = "sale")
    @Schema(description = "the selling rate of the currency for a money changer", example = "28.65000")
    private BigDecimal sellingRate;

    public CurrencyRate() {
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getBuyingRate() {
        return buyingRate;
    }

    public void setBuyingRate(BigDecimal buyingRate) {
        this.buyingRate = buyingRate;
    }

    public BigDecimal getSellingRate() {
        return sellingRate;
    }

    public void setSellingRate(BigDecimal sellingRate) {
        this.sellingRate = sellingRate;
    }

}